// Find users with letter s in their name or d in their last name. 

	db.users.find(
	{
		$or:[
			{firstName:{$regex:"s", $options:"$i"}},
			{lastName:{$regex:"d", $options:"$i"}}
	]},
	{
	    _id:0,
		firstName:1,
		lastName:1
	});

// Find users who are from the HR department and their age is greater than or equal 70.

	db.users.find({
		$and:[
			{department:"HR"},
			{age:{$gte:70}}
		]
	});
// Find users with the letter e in their firstname and has an age of less than or equal to 30.

		db.usersfind({
			$and:[
				{firstName:{$regex:"e"}},
				{age:{$lte:30}}
			]
		});